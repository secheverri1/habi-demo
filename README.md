# Habi Demo

Welcome to Habi Demo. This project contain a python Lambda function to be deployed on AWS and a API Gateway (REST). 

The project contains the following structure:

![Tree](images/structure.png "Tree")

## Getting started

To deploy the Lambda Function we have to use the pipeline, that contains the following steps:

** The fist step of the pipeline is to make terraform init to download all neccesary plugins.
1. **Terraform validate:** In this step we validate the terraform structure and its sintaxis.

![Validate](images/validate.png "Validate")

2. **Terraform plan:** In this step Terraform evaluates its configuration to determine which resources need to be deployed or changed comparing to the terraform state. 

![Plan](images/plan.png "Plan")

3. **Terraform apply:** In this step we use the planfile that we generated on the before step. With the apply command, Terraform will apply all the configuration displayed on the plan command. At the end of the output the pipeline prints the API URL.

![Apply](images/apply.png "Apply")

With this URL we can test our API using a tool like Postman, sendig the neccesary body in JSON format:

```
{
  "action": "square",
  "number": 4
}
```

After that, we have to configure the authentication method (AWS Signature), using the following credentials:

```
access_key: AKIAX2W6NVSRTT5SNC2N
secret_key: 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
```

![Authorization](images/authorization.png "Authorization")

> **_NOTE:_** this credential only have permission to execute the api.

Next, we are able to test the API:

![TestingAPI](images/testing-api.png "TestingAPI")
