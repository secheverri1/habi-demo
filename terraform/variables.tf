variable "region" {
  type        = string
  description = "The region in which to create/manage resources"
}

variable "account_id"{
  type        = string
  description = "The account ID in which to create/manage resources"
  default = "538411117731"
}
