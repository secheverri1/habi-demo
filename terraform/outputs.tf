output "deployment_invoke_url" {
  description = "Deployment invoke url"
  value       = module.api_gateway.deployment_invoke_url
}