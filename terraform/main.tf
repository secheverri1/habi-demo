provider "aws" {
  region = var.region
}

resource "aws_s3_bucket" "s3_terraform_state" {
  bucket = "habi-test-terraform-state"
}

resource "aws_s3_bucket_versioning" "s3_versioning" {
  bucket = aws_s3_bucket.s3_terraform_state.id
  versioning_configuration {    
      status = "Enabled"
  }
}

terraform {
  backend "s3" {
    bucket = "habi-test-terraform-state"
    key    = "state/terraform.tfstate"
    region = "us-east-1"
  }
}

module "lambda_function" {
  source = "./modules/lambda_function/"
}

module "api_gateway" {
  source = "./modules/api_gateway/"
  api_gateway_region = var.region
  api_gateway_account_id = var.account_id
  lambda_function_name = module.lambda_function.lambda_function_name
  lambda_function_arn = module.lambda_function.lambda_function_arn

  depends_on = [
    module.lambda_function
  ]
}
