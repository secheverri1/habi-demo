variable "rest_api_name" {
  type        = string
  description = "Name of the API Gateway"
  default     = "habi-api-gateway"
}

variable "api_gateway_region" {
  type        = string
} 

variable "api_gateway_account_id" {
  type        = string
} 

variable "lambda_function_name" {
  type        = string
  description = "The name of the Lambda function"
} 

variable "lambda_function_arn" {
  type        = string
  description = "The ARN of the Lambda function"
} 


variable "rest_api_stage_name" {
  type        = string
  description = "The name of the API Gateway stage"
  default     = "dev" 
}