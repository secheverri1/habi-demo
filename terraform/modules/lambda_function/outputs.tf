output "lambda_function_arn" {
  value = aws_lambda_function.habi_lambda_function.invoke_arn
}

output "lambda_function_name" {
  value = aws_lambda_function.habi_lambda_function.function_name
}